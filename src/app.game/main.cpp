
#include <game/game.hpp>
#include <game/log.hpp>

int main(int a_argc, char* a_argv[])
{
	try
	{
		std::vector<std::string> args;

		args.reserve(a_argc);
		for (int i = 0; i < a_argc; i++)
			args.push_back(std::string(a_argv[i]));

		game::settings settings(std::move(args));

		game::game game(settings);
		
		game.run();
	}
	catch (const std::exception& e)
	{
		LOG("uncaught exception: " << e.what());
	}

	return 0;
}