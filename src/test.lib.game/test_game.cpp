
#include <gtest/gtest.h>

#include <game/window.hpp>


TEST(instantiation, test_window)
{
	EXPECT_NO_THROW(game::window(256, 256, false));
}


TEST(exhaust_events, test_window)
{
	game::window window(256, 256, false);
	EXPECT_NO_THROW(window.exhaust_events());
	EXPECT_TRUE(window.exhaust_events());
}


TEST(get_renderer, test_window)
{
	game::window window(256, 256, false);
	EXPECT_NO_THROW(std::shared_ptr<game::renderer>(window.get_renderer()));
}
