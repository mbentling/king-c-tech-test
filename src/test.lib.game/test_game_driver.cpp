
#include <gtest/gtest.h>

#include <game/game_driver.hpp>


TEST(test_game_driver, remove_gem)
{
	const uint32_t width = 1, height = 2;
	const float time_step = 0.1f;
	const uint32_t gem = 1;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill column with gems
	board.set_slot(0, 0, game::slot(gem, 0, 0));
	board.set_slot(0, 1, game::slot(gem, 0, 0));
	EXPECT_EQ(game::slot(gem, 0, 0), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 0, 0), board.get_slot(0, 1));

	// step time and verify nothing happened
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 1));

	// remove the bottom gem
	board.set_slot(0, 1, game::slot());
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));

	// step time and verify the top gem started to fall
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 1));
}


TEST(test_game_driver, gem_to_rest)
{
	const uint32_t width = 1, height = 2;
	const float time_step = 0.1f;
	const uint32_t gem = 1;
	game::board board(width, height);
	game::game_driver game_driver;

	// add a moving gem to the bottom slot (it is moving into the slot)
	board.set_slot(0, 1, game::slot(gem, game::slot::state::moving, 1.0f));
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f), board.get_slot(0, 1));

	// step time and verify the gem moved but nothing else happened
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 1));

	// step time to where the gem almost comes to rest and verify progress
	game_driver.step_time(board, time_step * 2.5f);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot::state::moving, board.get_slot(0, 1).get_state());
	EXPECT_GT(0.05f, board.get_slot(0, 1).get_distance());

	// step time enough so the gem comes to rest and verify
	game_driver.step_time(board, time_step * 1.1f);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 1, 1), board.get_slot(0, 1));
}


TEST(test_game_driver, gem_to_stack)
{
	const uint32_t width = 1, height = 2;
	const float time_step = 0.1f;
	const uint32_t gem = 1;
	game::board board(width, height);
	game::game_driver game_driver;

	// add a moving gem to the top slot (it is moving into the slot) and a resting gem to the bottom
	board.set_slot(0, 0, game::slot(gem, game::slot::state::moving, 1.0f));
	board.set_slot(0, 1, game::slot(gem, 0, 0));

	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 0, 0), board.get_slot(0, 1));

	// step time and verify the gem moved but nothing else happened
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 1, 1), board.get_slot(0, 1));

	// step time to where the gem almost comes to rest and verify progress
	game_driver.step_time(board, time_step * 2.5f);
	EXPECT_EQ(game::slot::state::moving, board.get_slot(0, 0).get_state());
	EXPECT_GT(0.05f, board.get_slot(0, 0).get_distance());
	EXPECT_EQ(game::slot(gem, 1, 1), board.get_slot(0, 1));

	// step time enough so the gem comes to rest and verify
	game_driver.step_time(board, time_step * 1.1f);
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 1));
}


TEST(test_game_driver, gem_vanishing)
{
	const uint32_t width = 1, height = 2;
	const float time_step = 0.1f;
	const uint32_t gem_a = 1;
	const uint32_t gem_b = 2;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill column with gems and set the bottom most to vanishing
	board.set_slot(0, 0, game::slot(gem_a, 0, 0));
	board.set_slot(0, 1, game::slot(gem_b, game::slot::state::vanishing, 1.0));
	EXPECT_EQ(game::slot(gem_a, 0, 0), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem_b, game::slot::state::vanishing, 1.0f), board.get_slot(0, 1));

	// step time and verify vanishing progressed but nothing else happened
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(gem_a, 1, 1), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem_b, game::slot::state::vanishing, 1.0f - time_step), board.get_slot(0, 1));

	// step time enough for the game to vanish and verify the top gem started to move into the bottom slot
	game_driver.step_time(board, time_step * 9);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_TRUE(board.get_slot(0, 1).is_moving());
	EXPECT_NEAR(1.0f, board.get_slot(0, 1).get_distance(), 0.000001f);
}


/**
 * Added due to a bug where a gem vanished just below a moving gem would set the moving gems
 * distance to zero and essentially teleporting the gem into the slot
 */
TEST(test_game_driver, moving_gem_above_vanishing)
{
	const uint32_t width = 1, height = 2;
	const float time_step = 0.1f;
	const uint32_t gem_a = 1;
	const uint32_t gem_b = 2;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill column with gems and set the bottom most to vanishing
	board.set_slot(0, 0, game::slot(gem_a, game::slot::state::moving, 1.0f));
	board.set_slot(0, 1, game::slot(gem_b, game::slot::state::vanishing, 0.05f));
	EXPECT_EQ(game::slot(gem_a, game::slot::state::moving, 1.0f), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(gem_b, game::slot::state::vanishing, 0.05f), board.get_slot(0, 1));

	// step time and make sure the moving gem in slot[0,0] has moved a little bit and the slot [0,1] is empty
	game_driver.step_time(board, time_step);
	EXPECT_EQ(game::slot(gem_a, game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));
	return;
}


TEST(test_game_driver, large_delta_time)
{
	const uint32_t width = 1, height = 4;
	const float time_step = 0.1f;
	const uint32_t gem = 1;
	game::board board(width, height);
	game::game_driver game_driver;

	// add a moving gem to the top slot (it is moving into the slot) and a resting gem to the bottom
	board.set_slot(0, 0, game::slot(gem, game::slot::state::moving, 1.0f));
	board.set_slot(0, 3, game::slot(gem, 0, 0));

	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(gem, 0, 0), board.get_slot(0, 3));

	// step time and verify the gem is moving into slot [0,2] but nothing else happened
	game_driver.step_time(board, time_step * 5);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));
	EXPECT_EQ(game::slot::state::moving, board.get_slot(0, 2).get_state());
	EXPECT_NEAR(time_step * 5, board.get_slot(0, 2).get_distance(), 0.000001f);
	EXPECT_EQ(gem, board.get_slot(0, 3).get_gem());
}


TEST(test_game_driver, large_delta_time_to_rest)
{
	const uint32_t width = 1, height = 4;
	const float time_step = 0.1f;
	const uint32_t gem = 1;
	game::board board(width, height);
	game::game_driver game_driver;

	// add a moving gem to the top slot (it is moving into the slot) and a resting gem to the bottom
	board.set_slot(0, 0, game::slot(gem, game::slot::state::moving, 1.0f));
	board.set_slot(0, 3, game::slot(gem, 0, 0));

	EXPECT_EQ(game::slot(gem, game::slot::state::moving, 1.0f), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(gem, 0, 0), board.get_slot(0, 3));

	// step time and verify the gem is moving into slot [0,2] but nothing else happened
	game_driver.step_time(board, time_step * 31);
	EXPECT_EQ(game::slot(), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(gem, 1, 2), board.get_slot(0, 3));
}



TEST(test_game_driver, column_move)
{
	const uint32_t width = 1, height = 4;
	const float time_step = 0.1f;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill the column with gems
	for (size_t i = 0; i < board.get_height(); i++)
		board.set_slot(0, i, game::slot(i, 0, 0));

	game_driver.step_time(board, time_step);

	for (size_t i = 0; i < board.get_height(); i++)
	{
		EXPECT_EQ(game::slot::state::resting, board.get_slot(0, i).get_state());
		EXPECT_EQ(i, board.get_slot(0, i).get_gem());
	}

	// remove the bottom most
	board.at(0, board.get_height() - 1)->set_empty();

	// step time once and make sure the distance of each gem is the same
	game_driver.step_time(board, time_step);
	EXPECT_TRUE(board.get_slot(0, 0).is_empty());
	EXPECT_TRUE(board.get_slot(0, 1).is_moving());

	for (size_t i = 2; i < board.get_height(); i++)
	{
		EXPECT_TRUE(board.get_slot(0, i).is_moving());
		EXPECT_EQ(board.get_slot(0, 1).get_distance(), board.get_slot(0, i).get_distance());
	}

	// step time again and make sure the distance of each gem is the same
	game_driver.step_time(board, time_step);
	EXPECT_TRUE(board.get_slot(0, 0).is_empty());
	EXPECT_TRUE(board.get_slot(0, 1).is_moving());

	for (size_t i = 2; i < board.get_height(); i++)
	{
		EXPECT_TRUE(board.get_slot(0, i).is_moving());
		EXPECT_EQ(board.get_slot(0, 1).get_distance(), board.get_slot(0, i).get_distance());
	}

}


TEST(test_game_driver, swap_vertical)
{
	const uint32_t width = 1, height = 4;
	const float time_step = 0.1f;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill the column with gems
	for (size_t i = 0; i < board.get_height(); i++)
		board.set_slot(0, i, game::slot(i, 0, 0));

	game_driver.step_time(board, time_step);

	for (size_t i = 0; i < board.get_height(); i++)
	{
		EXPECT_EQ(game::slot(i, 1, 1), board.get_slot(0, i));
	}

	// set two gems as swapping
	const game::slot to_swap[2] = { board.get_slot(0, 1), board.get_slot(0, 2) };

	board.at(0, 1)->set_swapping(to_swap[1].get_gem(), game::slot::south, 1.0f);
	board.at(0, 2)->set_swapping(to_swap[0].get_gem(), game::slot::north, 1.0f);

	EXPECT_EQ(game::slot(to_swap[1].get_gem(), game::slot::state::moving, 1.0f), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), game::slot::state::moving, 1.0f), board.get_slot(0, 2));

	// step time and make sure the swap is progressing and that no gems try to fall through
	game_driver.step_time(board, time_step);

	EXPECT_EQ(game::slot(0, 1, 1), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(to_swap[1].get_gem(), game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(3, 1, 1), board.get_slot(0, 3));

	// step time enough to complete the swap and verify state
	game_driver.step_time(board, time_step * 9.5f);
	EXPECT_EQ(game::slot(0, 1, 1), board.get_slot(0, 0));
	EXPECT_EQ(game::slot(to_swap[1].get_gem(), 1, 1), board.get_slot(0, 1));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), 1, 1), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(3, 1, 1), board.get_slot(0, 3));
}


TEST(test_game_driver, swap_horizontal)
{
	const uint32_t width = 2, height = 4;
	const float time_step = 0.1f;
	game::board board(width, height);
	game::game_driver game_driver;

	// fill the column with gems
	for (size_t col = 0; col < board.get_width(); col++)
	{
		for (size_t row = 0; row < board.get_height(); row++)
			board.set_slot(col, row, game::slot(col * board.get_width() + row, 0, 0));
	}

	game_driver.step_time(board, time_step);

	for (size_t col = 0; col < board.get_width(); col++)
	{
		for (size_t row = 0; row < board.get_height(); row++)
		{
			EXPECT_EQ(game::slot(col * board.get_width() + row, 1, 1), board.get_slot(col, row));
		}
	}

	// set two gems as swapping
	const game::slot to_swap[2] = { board.get_slot(0, 2), board.get_slot(1, 2) };

	board.at(0, 2)->set_swapping(to_swap[1].get_gem(), game::slot::east, 1.0f);
	board.at(1, 2)->set_swapping(to_swap[0].get_gem(), game::slot::west, 1.0f);

	EXPECT_EQ(game::slot(to_swap[1].get_gem(), game::slot::state::moving, 1.0f), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), game::slot::state::moving, 1.0f), board.get_slot(1, 2));

	// step time and make sure the swap is progressing and that no gems try to fall through
	game_driver.step_time(board, time_step);

	for (size_t col = 0; col < board.get_width(); col++)
	{
		for (size_t row = 0; row < board.get_height(); row++)
		{
			if (row != 2) // row 2 has the swapping gems
			{
				EXPECT_EQ(game::slot(col * board.get_width() + row, 1, 1), board.get_slot(col, row));
			}
		}
	}

	EXPECT_EQ(game::slot(to_swap[1].get_gem(), game::slot::state::moving, 1.0f - time_step), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), game::slot::state::moving, 1.0f - time_step), board.get_slot(1, 2));

	// step time enough to complete the swap and verify state
	game_driver.step_time(board, time_step * 9.5f);

	for (size_t col = 0; col < board.get_width(); col++)
	{
		for (size_t row = 0; row < board.get_height(); row++)
		{
			if (row != 2) // row 2 has the swapping gems
			{
				EXPECT_EQ(game::slot(col * board.get_width() + row, 1, 1), board.get_slot(col, row));
			}
		}
	}

	EXPECT_EQ(game::slot(to_swap[1].get_gem(), 1, 1), board.get_slot(0, 2));
	EXPECT_EQ(game::slot(to_swap[0].get_gem(), 1, 1), board.get_slot(1, 2));
}
