#pragma once

#include <string>

namespace helper
{

	std::string get_binary_dir(const char* a_binary_path = nullptr);

}