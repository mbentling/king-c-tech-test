
#include "helper.hpp"

namespace helper
{

	std::string get_binary_dir(const char* a_binary_path)
	{
		static std::string dir;

		if (a_binary_path != nullptr)
		{
			std::string::size_type n = std::string::npos;

			dir = a_binary_path;
			n = dir.find_last_of('/');
			if (n == std::string::npos)
				n = dir.find_last_of('\\');
			dir = dir.substr(0, n);
		}

		return dir;
	}

}