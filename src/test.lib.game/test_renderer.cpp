
#include <gtest/gtest.h>

#include <game/exception.hpp>
#include <game/renderer.hpp>
#include <game/settings.hpp>
#include <game/window.hpp>

#include "helper.hpp"


TEST(instantiation, test_renderer)
{
	EXPECT_NO_THROW(std::shared_ptr<game::renderer>(game::window(256, 256, false).get_renderer()));
}


TEST(load_texture, test_renderer)
{
	game::window window(256, 256, false);
	game::settings settings({ helper::get_binary_dir() });

	std::shared_ptr<game::renderer> renderer(window.get_renderer());
	const std::string texture_path(settings.get_background_texture());

	EXPECT_THROW(std::shared_ptr<game::texture>(renderer->load_texture("bad_path")), game::sdl_error);
	EXPECT_NO_THROW(renderer->load_texture(texture_path));
	EXPECT_EQ(renderer->load_texture(texture_path)._Get(), renderer->load_texture(texture_path)._Get());
}
