
#include <gtest/gtest.h>

#include <game/board.hpp>
#include <game/gem_matcher.hpp>


/*
* Test matching on a board looking like this
* 1 1 1 1
* 1 2 2 3
* 2 2 3 4
*/
TEST(match, test_gem_matcher)
{
	std::vector<game::slot> slots =
	{
		// first column
		game::slot(1, 4, 2),
		game::slot(1, 1, 2),
		game::slot(2, 2, 1),

		// second column
		game::slot(1, 4, 1),
		game::slot(2, 2, 2),
		game::slot(2, 2, 2),

		// third column
		game::slot(1, 4, 1),
		game::slot(2, 2, 1),
		game::slot(3, 1, 1),

		// fourth column
		game::slot(1, 4, 1),
		game::slot(3, 1, 1),
		game::slot(4, 1, 1),
	};

	const size_t width = 4;
	const size_t height = 3;

	game::board board(width, height);
	game::gem_matcher gem_matcher;

	{
		std::vector<game::slot>::iterator source_slot = slots.begin();

		for (auto board_slot = board.begin(); board_slot != board.end(); board_slot++, source_slot++)
		{
			board_slot->set_gem(source_slot->get_gem());
			board_slot->set_score(0, game::slot::axis::axis_x);
			board_slot->set_score(0, game::slot::axis::axis_y);
		}
	}

	gem_matcher.match(board);

	{
		std::vector<game::slot>::iterator source_slot = slots.begin();

		for (auto board_slot = board.begin(); board_slot != board.end(); board_slot++, source_slot++)
		{
			EXPECT_EQ(*source_slot, *board_slot);
		}
	}
}


TEST(match_top, test_gem_matcher)
{
	game::board board(1, 4);
	game::gem_matcher gem_matcher;

	board.set_slot(0, 0, game::slot(1, 0, 0));
	board.set_slot(0, 1, game::slot(1, 0, 0));
	board.set_slot(0, 2, game::slot(1, 0, 0));
	board.set_slot(0, 3, game::slot(2, 0, 0));

	gem_matcher.match(board);

	EXPECT_EQ(3, board.get_slot(0, 0).get_score(3));
	EXPECT_EQ(3, board.get_slot(0, 1).get_score(3));
	EXPECT_EQ(3, board.get_slot(0, 2).get_score(3));
	EXPECT_EQ(0, board.get_slot(0, 3).get_score(3));
}
