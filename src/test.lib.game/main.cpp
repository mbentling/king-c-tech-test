
#include <gtest/gtest.h>

#include "helper.hpp"


int main(int a_argc, char** a_argv) {
	assert(a_argc > 0);
	helper::get_binary_dir(a_argv[0]);
	::testing::InitGoogleTest(&a_argc, a_argv);
	return RUN_ALL_TESTS();
}
