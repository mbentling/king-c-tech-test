
#include <gtest/gtest.h>

#include <game/board.hpp>
#include <game/gem_generator.hpp>
#include <game/gem_matcher.hpp>


TEST(fill_board, test_gem_generator)
{
	const uint32_t num_gems = 5, threshold = 3;
	game::gem_generator generator(num_gems);
	game::gem_matcher matcher;
	game::board board(8, 8);
	
	generator.fill_board(board, threshold);
	matcher.match(board);

	for (auto slot = board.begin(); slot != board.end(); slot++)
	{
		EXPECT_EQ(game::slot::state::resting, slot->get_state());
		EXPECT_GE(slot->get_gem(), uint32_t(0));
		EXPECT_LE(slot->get_gem(), num_gems - 1);
		EXPECT_LE(slot->get_score(threshold), threshold - 1);
	}

}

