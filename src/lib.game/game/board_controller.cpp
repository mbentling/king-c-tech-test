
#include "board_controller.hpp"

#include "board.hpp"
#include "board_view.hpp"

namespace game
{

	board_controller::board_controller(int a_top, int a_left, int a_slot_size, float a_mouse_click_threshold)
		: m_selected_gem(-1, -1)
		, m_top(a_top)
		, m_left(a_left)
		, m_slot_size(a_slot_size)
		, m_is_swapping(false)
		, m_can_drag_swap(true)
		, m_mouse_click_threshold(a_mouse_click_threshold)
	{}


	void board_controller::update(board& a_board)
	{
		if (m_is_swapping)
		{
			bool is_ok = false;

			for (int i = 0; i < 2 && is_ok == false; i++)
			{
				switch (a_board.get_slot(m_swapping_slots[i].m_x, m_swapping_slots[i].m_y).get_state())
				{
				case slot::state::vanishing:
					m_is_swapping = false;
					is_ok = true;
					break;

				case slot::state::resting:
					m_is_swapping = false;
					break;
				}
			}

			// if not swapping anymore we check the validity of the swap
			if (!m_is_swapping)
			{
				if (!is_ok) // swap back without setting the swap state in the controller
				{
					board::slot_iterator slot[2] =
					{
						a_board.at(m_swapping_slots[0].m_x, m_swapping_slots[0].m_y),
						a_board.at(m_swapping_slots[1].m_x, m_swapping_slots[1].m_y)
					};
					
					slot::gem_t gem = slot[0]->get_gem();
					slot::swap_direction direction = slot[1]->get_swap_from();

					slot[0]->set_swapping(slot[1]->get_gem(), slot[0]->get_swap_from(), 1.0f);
					slot[1]->set_swapping(gem, direction, 1.0f);
				}
			}
		}
		
	}


	void board_controller::handle_mouse_drag(board& a_board, const point<int>& a_start, const point<int>& a_last)
	{
		if (m_can_drag_swap)
		{
			// translate current coordinates into board coordinates
			point<int> from((a_start.m_x - m_left) / m_slot_size, (a_start.m_y - m_top) / m_slot_size);
			point<int> to((a_last.m_x - m_left) / m_slot_size, (a_last.m_y - m_top) / m_slot_size);

			if (from.m_x >= 0 && static_cast<size_t>(from.m_x) < a_board.get_width() && from.m_y >= 0 && static_cast<size_t>(from.m_y) < a_board.get_height() &&
				to.m_x >= 0 && static_cast<size_t>(to.m_x) < a_board.get_width() && to.m_y >= 0 && static_cast<size_t>(to.m_y) < a_board.get_height())
			{
				try_swap(a_board, from, to);
			}
		}
	}


	void board_controller::handle_mouse_up(board& a_board, const point<int>& a_last, float a_time_pressed)
	{
		if (a_time_pressed <= m_mouse_click_threshold && m_can_drag_swap)
		{
			// translate click into board coordinates
			point<int> slot_clicked((a_last.m_x - m_left) / m_slot_size, (a_last.m_y - m_top) / m_slot_size);

			if (slot_clicked.m_x >= 0 && static_cast<size_t>(slot_clicked.m_x) < a_board.get_width() && slot_clicked.m_y >= 0 && static_cast<size_t>(slot_clicked.m_y) < a_board.get_height())
			{
				if (slot_clicked == m_selected_gem)
				{
					m_selected_gem.set(-1, -1);
				}
				else if (m_selected_gem.m_x < 0)
				{
					if (a_board.get_slot(slot_clicked.m_x, slot_clicked.m_y).has_gem())
						m_selected_gem.set(slot_clicked.m_x, slot_clicked.m_y);
				}
				else
				{
					try_swap(a_board, slot_clicked, m_selected_gem);
					m_selected_gem.set(-1, -1);
				}
			}
		}
		else
		{
			m_selected_gem.set(-1, -1);
		}

		m_can_drag_swap = true;
	}


	void board_controller::try_swap(board& a_board, const point<int>& a_from, const point<int>& a_to)
	{
		if (std::abs(a_from.m_x - a_to.m_x) + std::abs(a_from.m_y - a_to.m_y) == 1)
		{
			board::slot_iterator slot[2] =
			{
				a_board.at(a_to.m_x, a_to.m_y),
				a_board.at(a_from.m_x, a_from.m_y)
			};

			if (slot[0]->has_gem() && slot[1]->has_gem())
			{
				slot::swap_direction direction[2] =
				{
					slot::get_swap_direction(a_from, a_to),
					slot::get_swap_direction(a_to, a_from)
				};

				slot::gem_t gem0 = slot[0]->get_gem();

				slot[0]->set_swapping(slot[1]->get_gem(), direction[0], 1.0f);
				slot[1]->set_swapping(gem0, direction[1], 1.0f);

				m_can_drag_swap = false;
				m_is_swapping = true;
				m_swapping_slots[0] = a_from;
				m_swapping_slots[1] = a_to;
			}
		}
	}

}