#pragma once

#include "board.hpp"

namespace game
{

	class gem_matcher
	{
	public:
		gem_matcher(void);

		void match(board& a_board);

		void match(const board::slot_iterator a_from, const board::slot_iterator a_to, const size_t a_stride, slot::axis a_axis);
	};
}