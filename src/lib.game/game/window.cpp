
#include "window.hpp"

#include "exception.hpp"
#include "renderer.hpp"

#include <SDL.h>

namespace game
{

	window::window(uint32_t a_width, uint32_t a_height, bool a_show)
		: m_window(nullptr)
		, m_renderer(nullptr)
	{
		const SDL_WindowFlags show = a_show ? SDL_WINDOW_SHOWN : SDL_WINDOW_HIDDEN;

		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
			throw sdl_error(SDL_GetError());

		m_window = SDL_CreateWindow("King C++ Tech Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, a_width, a_height, show);

		if (m_window == nullptr)
			throw sdl_error(SDL_GetError());
	}
	
	
	window::~window(void)
	{
		if (m_window)
			SDL_DestroyWindow(m_window);

		SDL_Quit();
	}


	void window::set_title(const std::string& a_title)
	{
		SDL_SetWindowTitle(m_window, a_title.c_str());
	}


	bool window::exhaust_events(void)
	{
		SDL_Event evt;

		while (SDL_PollEvent(&evt) != 0)
		{
			switch (evt.type)
			{
			case SDL_MOUSEBUTTONDOWN:
				if (evt.button.button == SDL_BUTTON_LEFT)
				{
					m_mouse_button_start = SDL_GetPerformanceCounter();
					m_mouse_state.button_down(point<int>(evt.button.x, evt.button.y));
				}
				break;

			case SDL_MOUSEMOTION:
				if (m_mouse_state.is_down())
				{
					uint64_t now = SDL_GetPerformanceCounter();
					float delta_time = (now - m_mouse_button_start) / (float)SDL_GetPerformanceFrequency();
					m_mouse_state.move(point<int>(evt.motion.x, evt.motion.y), delta_time);
				}
				break;

			case SDL_MOUSEBUTTONUP:
				if (evt.button.button == SDL_BUTTON_LEFT)
				{
					uint64_t now = SDL_GetPerformanceCounter();
					float delta_time = (now - m_mouse_button_start) / (float)SDL_GetPerformanceFrequency();
					m_mouse_state.button_released(point<int>(evt.button.x, evt.button.y), delta_time);
				}
				break;

			case SDL_QUIT:
				return false;
			}
		}

		return true;
	}


	std::weak_ptr<renderer> window::get_renderer(void)
	{
		if (!m_renderer)
			m_renderer = std::make_shared<renderer>(m_window);

		return m_renderer;
	}

}