#pragma once

#include "slot.hpp"

#include <cstdint>
#include <map>
#include <stdexcept>


namespace game
{

	bool slot::operator==(const slot& a_slot) const
	{
		if (m_state != a_slot.m_state)
			return false;

		switch (m_state)
		{
		case state::empty:
			return true;

		case state::resting:
			return m_gem == a_slot.m_gem &&
				m_score_x == a_slot.m_score_x &&
				m_score_y == a_slot.m_score_y;

		case state::moving:
			return m_gem == a_slot.m_gem &&
				m_distance == a_slot.m_distance;

		case state::vanishing:
			return m_gem == a_slot.m_gem &&
				m_distance == a_slot.m_distance;

		default:
			throw std::runtime_error("unexpected slot state");
		}

		return true;
	}


	std::ostream& operator<<(std::ostream& a_os, const slot& a_slot)
	{
		a_os << "[ " << a_slot.m_state << " ";

		switch (a_slot.m_state)
		{
		case slot::state::empty:
			break;

		case slot::state::resting:
			a_os << "gem:" << a_slot.m_gem << " score_x:" << a_slot.m_score_x << " score_y:" << a_slot.m_score_y << " ";
			break;

		case slot::state::moving:
		case slot::state::vanishing:
			a_os << "gem:" << a_slot.m_gem << " distance:" << a_slot.m_distance << " ";
			break;

		default:
			throw std::runtime_error("unexpected slot state");
		}

		return a_os << "]";
	}


	std::ostream& operator<<(std::ostream& a_os, const slot::state& a_state)
	{
		static const std::map<slot::state, const char*> state_to_string =
		{
			{ slot::state::empty, "empty" },
			{ slot::state::resting, "resting" },
			{ slot::state::moving, "moving" },
			{ slot::state::vanishing, "vanishing" }
		};

		auto it = state_to_string.find(a_state);

		if (it != state_to_string.end())
			a_os << it->second;
		else
			a_os << "unexpected slot state";

		return a_os;
	}

}