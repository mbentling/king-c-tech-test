
#include "gem_matcher.hpp"

namespace game
{

	gem_matcher::gem_matcher(void)
	{}


	void gem_matcher::match(board& a_board)
	{
		for (size_t col = 0; col < a_board.get_width(); col++)
			match(a_board.begin_column(col), a_board.last_column(col), 1, slot::axis::axis_y);

		for (size_t row = 0; row < a_board.get_height(); row++)
			match(a_board.begin_row(row), a_board.last_row(row), a_board.get_height(), slot::axis::axis_x);
	}


	/**
	 * Matches slots using a stride to iterate to the next slot
	 * @param a_from The slot to start with
	 * @param a_to The slot to end with and it is included
	 * @param a_stride How far there is between slots to match
	 * @param a_sequence Where to store the match counts in each slot
	 */
	void gem_matcher::match(const board::slot_iterator a_from, const board::slot_iterator a_to, const size_t a_stride, slot::axis a_axis)
	{
		board::slot_iterator current(a_from);	// the current slot we are matching against
		board::slot_iterator last;				// the last slot in the sequence matching current
		board::slot_iterator next;
		int count;

		do
		{
			last = current;
			count = 0;

			// find the last slot matching current
			if (current->has_gem())
			{

				while (last != a_to && last->has_gem(current->get_gem()))
					last += a_stride;
				//last += a_stride;

				if (last != a_to || !last->has_gem(current->get_gem()))
					last -= a_stride;

				count = (last - current) / a_stride + 1;
			}

			// update sequence count on matching slots
			current->set_score(count, a_axis);
			while (current != last)
			{
				current += a_stride;
				current->set_score(count, a_axis);
			}

			// if we have more slots to match, advance current and initialize
			if (last != a_to)
			{
				current += a_stride;
				if (current->has_gem())
					current->set_score(1, a_axis);
				else
					current->set_score(0, a_axis);
			}

		} while (current != a_to);
	}

}