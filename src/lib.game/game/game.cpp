
#include "game.hpp"

#include "game_play.hpp"
#include "game_title.hpp"

#include <SDL_timer.h>

namespace game
{

	game::game(const settings& a_settings)
		: m_window(a_settings.get_window_width(), a_settings.get_window_height(), true)
		, m_settings(a_settings)
	{
	}


	game::~game(void)
	{}


	void game::run(void)
	{
		std::shared_ptr<game_state> state(std::make_shared<game_title>(m_window, m_settings, m_settings.get_highscore()));
		int num_exceptions = 0;
		Uint64 last_time, now;
		float delta_time;
		
		last_time = SDL_GetPerformanceCounter();
		while (m_window.exhaust_events())
		{
			try
			{
				now = SDL_GetPerformanceCounter();
				delta_time = (now - last_time) / (float)SDL_GetPerformanceFrequency();
				last_time = now;

				std::shared_ptr<game_state> next_state = state->update(m_window, m_settings, delta_time);
				if (next_state)
					state = next_state;
				num_exceptions = 0;
			}
			catch (std::exception& e)
			{
				std::cout << "caught std::exception: " << e.what() << std::endl;
				if (++num_exceptions > 1)
				{
					std::cout << "aborting" << std::endl;
					return;
				}
			}
		}
	}

}