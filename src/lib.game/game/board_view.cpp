
#include "board_view.hpp"

#include "board.hpp"
#include "board_controller.hpp"
#include "renderer.hpp"
#include "slot.hpp"
#include "texture.hpp"

#include <iostream>

namespace game
{

	board_view::board_view(std::vector<std::weak_ptr<texture>>&& a_gem_textures, int a_top, int a_left, int a_slot_size, int a_top_margin)
		: m_gem_textures(std::move(a_gem_textures))
		, m_top(a_top)
		, m_left(a_left)
		, m_slot_size(a_slot_size)
		, m_top_margin(a_top_margin)
		, m_time_selected(0.0f)
	{}


	void board_view::render(std::weak_ptr<renderer> a_renderer, board& a_board, board_controller& a_controller, float a_dt)
	{
		const point<int> selected_gem = a_controller.get_last_selected_gem();
		std::shared_ptr<renderer> renderer(a_renderer);
		uint32_t gem, x, y;
		float scale;

		try
		{
			renderer->set_clip_rect(m_top - m_top_margin, m_left, m_slot_size * a_board.get_width(), m_slot_size * a_board.get_height() + m_top_margin);

			for (size_t col = 0; col < a_board.get_width(); col++)
			{
				for (size_t row = 0; row < a_board.get_height(); row++)
				{
					const slot& current = a_board.get_slot(col, row);
					switch (current.get_state())
					{
					case slot::state::resting:
						gem = current.get_gem();
						assert(gem >= 0 && gem < m_gem_textures.size());
						x = m_left + col * m_slot_size;
						y = m_top + row * m_slot_size;
						scale = 1.0f;

						if (a_controller.has_selected_gem() && col == selected_gem.m_x && row == selected_gem.m_y)
						{
							scale += sinf(m_time_selected * 12) * 0.1f;
							x = static_cast<uint32_t>((x + m_slot_size / 2) - (scale * m_slot_size / 2) + 0.5f);
							y = static_cast<uint32_t>((y + m_slot_size / 2) - (scale * m_slot_size / 2) + 0.5f);
						}

						renderer->render(std::shared_ptr<texture>(m_gem_textures[gem]), y, x, scale);
						break;

					case slot::state::moving:
						gem = current.get_gem();
						assert(gem >= 0 && gem < m_gem_textures.size());
						x = m_left + col * m_slot_size;
						y = m_top + row * m_slot_size;

						if (current.is_swapping())
						{
							switch (current.get_swap_from())
							{
							case slot::north:
								y -= static_cast<int32_t>(current.get_distance() * m_slot_size);
								break;

							case slot::east:
								x += static_cast<int32_t>(current.get_distance() * m_slot_size);
								break;

							case slot::south:
								y += static_cast<int32_t>(current.get_distance() * m_slot_size);
								break;

							case slot::west:
								x -= static_cast<int32_t>(current.get_distance() * m_slot_size);
								break;
							}
						}
						else
						{
							y -= static_cast<int32_t>(current.get_distance() * m_slot_size);
						}

						renderer->render(std::shared_ptr<texture>(m_gem_textures[gem]), y, x);
						break;

					case slot::state::vanishing:
						gem = current.get_gem();
						assert(gem >= 0 && gem < m_gem_textures.size());
						x = y = static_cast<int32_t>((1.0f - current.get_distance()) * m_slot_size * 0.5f);
						x += m_left + col * m_slot_size;
						y += m_top + row * m_slot_size;
						renderer->render(std::shared_ptr<texture>(m_gem_textures[gem]), y, x, current.get_distance());
						break;
					}
				}
			}

			renderer->clear_clip_rect();

			if (a_controller.has_selected_gem())
				m_time_selected += a_dt;
			else
				m_time_selected = 0.0f;
		}
		catch (const std::exception& e)
		{
			renderer->clear_clip_rect();
			throw e;
		}
	}

}