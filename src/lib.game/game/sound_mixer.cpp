
#include "sound_mixer.hpp"

#include "exception.hpp"
#include "music.hpp"
#include "sound.hpp"

#include <SDL_mixer.h>

namespace game
{

	sound_mixer::sound_mixer(void)
	{
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) != 0)
			throw sdl_error("failed to open audio mixer");
	}


	sound_mixer::~sound_mixer(void)
	{
		Mix_CloseAudio();
	}


	bool sound_mixer::is_playing_music(void) const
	{
		return Mix_PlayingMusic() == 1;
	}


	std::weak_ptr<music> sound_mixer::get_music(const std::string& a_file)
	{
		auto cached_music = m_music.find(a_file);
		
		if (cached_music != m_music.end())
			return cached_music->second;

		Mix_Music* mix_music = Mix_LoadMUS(a_file.c_str());

		if (mix_music == nullptr)
			throw sdl_error("failed to load music " + a_file);

		return m_music[a_file] = std::make_shared<music>(mix_music);
	}


	std::weak_ptr<sound> sound_mixer::get_sound(const std::string& a_file)
	{
		auto cached_sound = m_sound.find(a_file);

		if (cached_sound != m_sound.end())
			return cached_sound->second;

		Mix_Chunk* chunk = Mix_LoadWAV(a_file.c_str());
		if (chunk == nullptr)
			throw sdl_error("failed to load sound " + a_file);

		return m_sound[a_file] = std::make_shared<sound>(chunk);
	}


	void sound_mixer::fade_out_music(float a_seconds)
	{
		if (Mix_FadeOutMusic(static_cast<int>(a_seconds * 1000)) == 0)
			throw sdl_error("failed to fade out music ");
	}


	void sound_mixer::stop_music(void)
	{
		if (Mix_HaltMusic() != 0)
			throw sdl_error("failed to stop music");
	}
}