#pragma once

#include <cstdint>
#include <memory>
#include <string>

struct SDL_Renderer;
struct _TTF_Font;
typedef _TTF_Font TTF_Font;

namespace game
{

	class texture;

	class font
	{
	public:
		font(TTF_Font* a_font);

		~font(void);

		std::shared_ptr<texture> render(SDL_Renderer* a_renderer, const std::string& a_text, float a_red, float a_green, float a_blue, float a_alpha);

	private:
		TTF_Font* m_font;
	};
}