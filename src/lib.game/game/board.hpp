#pragma once

#include "slot.hpp"

#include <vector>

namespace game
{

	template<class T> struct point;

	class board
	{
	public:
		typedef std::vector<slot> slot_container;
		typedef slot_container::iterator slot_iterator;

	public:
		board(void);

		board(size_t a_width, size_t a_height);

		void reset(size_t a_width, size_t a_height);

		size_t get_width(void) const { return m_width; }

		size_t get_height(void) const { return m_height; }

		inline slot_iterator begin_column(size_t a_column) { return m_slots.begin() + a_column * m_height; }

		inline slot_iterator last_column(size_t a_column) { return begin_column(a_column) + (m_height - 1); }

		inline slot_iterator begin_row(size_t a_row) { return m_slots.begin() + a_row; }

		inline slot_iterator last_row(size_t a_row) { return begin_column(m_width - 1) + a_row; }

		inline slot_iterator begin(void) { return m_slots.begin(); }

		inline slot_iterator end(void) { return m_slots.end(); }

		inline slot_iterator at(size_t a_column, size_t a_row) { return m_slots.begin() + a_column * m_height + a_row; }

		inline void set_slot(size_t a_column, size_t a_row, const slot& a_slot) { m_slots[a_column * m_height + a_row] = a_slot; }

		inline const slot& get_slot(size_t a_column, size_t a_row) const { return m_slots[a_column * m_height + a_row]; }

		inline slot_iterator left(slot_iterator a_iterator) const { return a_iterator - m_width; }

		inline slot_iterator right(slot_iterator a_iterator) const { return a_iterator + m_width; }

		inline slot_iterator up(slot_iterator a_iterator) const { return a_iterator - 1; }

		inline slot_iterator down(slot_iterator a_iterator) const { return a_iterator + 1; }

		bool is_at_rest(void) const;

	private:
		slot_container m_slots;
		size_t m_width;
		size_t m_height;
	};

}