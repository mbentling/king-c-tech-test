#pragma once

#include "point.hpp"

#include <memory>
#include <map>
#include <string>

struct SDL_Renderer;
struct SDL_Window;
struct _TTF_Font;

typedef _TTF_Font TTF_Font;

namespace game
{

	class font;
	class texture;

	class renderer
	{
	public:
		renderer(SDL_Window* a_window);

		~renderer(void);

		void flush(void);

		void render(std::shared_ptr<texture> a_texture, int32_t a_top, int32_t a_left, float a_scale = 1.0f);

		std::shared_ptr<texture> render_text(std::shared_ptr<font> a_font, const std::string& a_text, float a_red, float a_green, float a_blue, float a_alpha);

		std::weak_ptr<font> load_font(const std::string& a_fontpath, int a_fontsize);

		std::weak_ptr<texture> load_texture(const std::string& a_path);

		void set_clip_rect(int32_t a_top, int32_t a_left, int32_t a_width, int32_t a_height);

		void clear_clip_rect(void);

	private:
		std::map<std::string, std::map<int,std::shared_ptr<font>>> m_fonts;
		std::map<std::string, std::shared_ptr<texture>> m_textures;
		SDL_Renderer* m_renderer;
	};
}