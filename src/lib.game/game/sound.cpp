
#include "sound.hpp"

#include "exception.hpp"

#include <SDL_mixer.h>

namespace game
{

	sound::sound(Mix_Chunk* a_chunk)
		: m_chunk(a_chunk)
	{}


	sound::~sound(void)
	{
		if (m_chunk != nullptr)
			Mix_FreeChunk(m_chunk);
	}


	void sound::play(void)
	{
		if (m_chunk != nullptr)
		Mix_PlayChannel(-1, m_chunk, 0);
	}
}