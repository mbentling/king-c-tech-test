#pragma once

#include "point.hpp"

#include <memory>
#include <vector>

namespace game
{

	class board;
	class board_controller;
	class renderer;
	class texture;

	class board_view
	{
	public:
		board_view(std::vector<std::weak_ptr<texture>>&& a_gem_textures, int a_top, int a_left, int a_slot_size, int a_top_margin);

		inline const int get_top(void) const { return m_top; }

		inline const int get_left(void) const { return m_left; }

		inline const int get_slot_size(void) const { return m_slot_size; }

		void render(std::weak_ptr<renderer> a_renderer, board& a_board, board_controller& a_controller, float a_dt);

		void handle_mouse_drag(board& a_board, int a_start_x, int a_start_y, int a_last_x, int a_last_y);

		void handle_mouse_click(board& a_board, int a_last_x, int a_last_y);

	private:
		std::vector<std::weak_ptr<texture>> m_gem_textures;
		int m_top;
		int m_left;
		int m_top_margin;
		int m_slot_size;

		float m_time_selected;
	};

}