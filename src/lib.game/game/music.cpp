
#include "music.hpp"

#include "exception.hpp"

#include <SDL_mixer.h>


namespace game
{

	music::music(Mix_Music* a_music)
		: m_music(a_music)
	{}


	music::~music(void)
	{
		if (m_music != nullptr)
			Mix_FreeMusic(m_music);
	}


	void music::play(int a_num_loops)
	{
		if (m_music != nullptr)
		{
			if (Mix_PlayMusic(m_music, a_num_loops) != 0)
				throw sdl_error("failed to play music");
		}
	}

}