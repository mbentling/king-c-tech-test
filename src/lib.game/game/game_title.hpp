#pragma once

#include "game_state.hpp"

#include "sound_mixer.hpp"
#include "window.hpp"

#include <vector>

namespace game
{

	class settings;
	class texture;
	class window;

	class game_title : public game_state
	{
	public:
		game_title(window& a_window, const settings& a_settings, uint32_t a_highscore = 0, uint32_t a_lastscore = 0);

		virtual ~game_title(void);

		virtual std::shared_ptr<game_state> update(window& a_window, const settings& a_settings, float a_dt);

	protected:
		void handle_mouse(window& a_window);

		void render_text(renderer& a_renderer, const settings& a_settings);

	private:
		sound_mixer m_sound_mixer;
		mouse_state m_last_mouse_state;
		uint32_t m_highscore;
		uint32_t m_lastscore;
		float m_fade_out;

		std::weak_ptr<texture> m_background;

		std::vector<std::shared_ptr<texture>> m_text;
	};

}