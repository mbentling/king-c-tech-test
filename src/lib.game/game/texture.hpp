#pragma once

#include <cstdint>
#include <string>

struct SDL_Texture;
struct SDL_Renderer;

namespace game
{

	class texture
	{
	public:
		texture(SDL_Texture* a_texture);

		~texture(void);

		void render(SDL_Renderer* a_renderer, int32_t a_top, int32_t a_left, float a_scale);

		int get_width(void) const { return m_width; }

		int get_height(void) const { return m_height; }

	private:
		SDL_Texture* m_texture;
		int m_width;
		int m_height;
	};
}