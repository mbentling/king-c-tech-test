
#include "game_title.hpp"

#include "game_play.hpp"
#include "font.hpp"
#include "music.hpp"
#include "renderer.hpp"
#include "settings.hpp"
#include "texture.hpp"
#include "window.hpp"

#include <SDL_mixer.h>

#include <sstream>


namespace game
{

	game_title::game_title(window& a_window, const settings& a_settings, uint32_t a_highscore, uint32_t a_lastscore)
		: m_fade_out(-1.0f)
		, m_highscore(a_highscore)
		, m_lastscore(a_lastscore)
	{
		std::shared_ptr<renderer> renderer(a_window.get_renderer());

		// load background
		m_background = renderer->load_texture(a_settings.get_background_texture());

		std::shared_ptr<music> title_music(m_sound_mixer.get_music(a_settings.get_title_music()));
		
		title_music->play();

		std::shared_ptr<font> fnt(renderer->load_font(a_settings.get_font(), 30));
		std::ostringstream os;

		if (a_lastscore != 0)
		{
			os << "Last score: " << a_lastscore;
			m_text.push_back(renderer->render_text(fnt, os.str(), 1.0f, 1.0f, 1.0f, 1.0f));
			os.str("");
		}

		if (a_highscore != 0)
		{
			os << "High score: " << a_highscore;
			m_text.push_back(renderer->render_text(fnt, os.str(), 1.0f, 1.0f, 1.0f, 1.0f));
			os.str("");
		}

		m_text.push_back(renderer->render_text(fnt, "Press mouse button to start", 1.0f, 1.0f, 1.0f, 1.0f));
	}


	game_title::~game_title(void)
	{}


	std::shared_ptr<game_state> game_title::update(window& a_window, const settings& a_settings, float a_dt)
	{
		std::shared_ptr<renderer> renderer(a_window.get_renderer());
		
		renderer->render(std::shared_ptr<texture>(m_background), 0, 0);

		render_text(*renderer, a_settings);

		renderer->flush();

		handle_mouse(a_window);

		if (!m_sound_mixer.is_playing_music())
			return std::make_shared<game_play>(a_window, a_settings, m_highscore);

		return std::shared_ptr<game_state>();

	}


	void game_title::handle_mouse(window& a_window)
	{
		if (!a_window.get_mouse_state().is_down() && m_last_mouse_state.is_down() && m_fade_out < 0.0f)
		{
			m_fade_out = 1.0f;
			m_sound_mixer.fade_out_music(1.0f);
		}

		m_last_mouse_state = a_window.get_mouse_state();
	}


	void game_title::render_text(renderer& a_renderer, const settings& a_settings)
	{
		int total_height = 0;

		for (auto text = m_text.begin(); text != m_text.end(); text++)
			total_height += (*text)->get_height() * 2;

		int top = a_settings.get_board_top() + (a_settings.get_num_gems_vertical() * a_settings.get_slot_width() - total_height) / 2;

		for (auto text = m_text.begin(); text != m_text.end(); text++)
		{
			int left = a_settings.get_board_left() + (a_settings.get_num_gems_horizontal() * a_settings.get_slot_width() - (*text)->get_width()) / 2;
			a_renderer.render(*text, top, left);
			top += (*text)->get_height() * 2;
		}
	}

}