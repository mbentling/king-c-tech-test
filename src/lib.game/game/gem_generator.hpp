#pragma once

#include <cstdint>
#include <random>

namespace game
{

	class board;

	class gem_generator
	{
	public:
		gem_generator(uint32_t a_num_gems);

		void fill_board(board& a_board, uint32_t a_sequence_limit);

		inline uint32_t next_gem(void) { return m_gem_distribution(m_generator); }

		uint32_t count_row_sequence(board& a_board, uint32_t a_col, uint32_t a_row);

		uint32_t count_column_sequence(board& a_board, uint32_t a_col, uint32_t a_row);

		uint32_t get_num_gems(void) const { return m_num_gems; }

	private:
		std::uniform_int_distribution<uint32_t> m_gem_distribution;
		std::mt19937 m_generator;
		uint32_t m_num_gems;
	};
}