
#include "renderer.hpp"

#include "exception.hpp"
#include "font.hpp"
#include "texture.hpp"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <cassert>
#include <memory>


namespace game
{

	renderer::renderer(SDL_Window* a_window)
		: m_renderer(SDL_CreateRenderer(a_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))
	{
		if (m_renderer == nullptr)
			throw sdl_error(SDL_GetError());

		if(TTF_WasInit() == 0)
			TTF_Init();
	}


	renderer::~renderer(void)
	{
		if (m_renderer)
			SDL_DestroyRenderer(m_renderer);
	}


	void renderer::flush(void)
	{
		SDL_RenderPresent(m_renderer);
	}


	std::weak_ptr<font> renderer::load_font(const std::string& a_fontpath, int a_fontsize)
	{
		auto font_by_file = m_fonts.find(a_fontpath);

		if (font_by_file != m_fonts.end())
		{
			auto font_by_size = font_by_file->second.find(a_fontsize);
			if (font_by_size != font_by_file->second.end())
				return font_by_size->second;
		}

		TTF_Font* ttf_font = TTF_OpenFont(a_fontpath.c_str(), a_fontsize);
		if (ttf_font == nullptr)
			throw sdl_error(std::string("failed to open font ") + a_fontpath);

		std::shared_ptr<font> loaded_font(std::make_shared<font>(ttf_font));

		if (font_by_file == m_fonts.end())
			m_fonts[a_fontpath].insert(std::make_pair(a_fontsize, loaded_font));
		else
			font_by_file->second.insert(std::make_pair(a_fontsize, loaded_font));

		return loaded_font;
	}


	std::weak_ptr<texture> renderer::load_texture(const std::string& a_path)
	{
		auto cached_texture = m_textures.find(a_path);
		
		if (cached_texture != m_textures.end())
			return cached_texture->second;

		uint32_t flags = IMG_INIT_JPG | IMG_INIT_PNG;
		uint32_t res = IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
		SDL_Surface* surface = IMG_Load(a_path.c_str());

		if (surface == nullptr)
			throw sdl_error(std::string("failed to load texture: ") + a_path);

		SDL_Texture* sdl_texture = SDL_CreateTextureFromSurface(m_renderer, surface);

		SDL_FreeSurface(surface);

		if (sdl_texture == nullptr)
			throw sdl_error(std::string("failed to create texture from surface: ") + a_path);

		auto pair = m_textures.insert(std::make_pair(a_path, std::make_shared<texture>(sdl_texture)));

		if (pair.second)
			SDL_Log("loading texture '%s' from %s", a_path.c_str(), a_path.c_str());

		return pair.first->second;
	}


	void renderer::render(std::shared_ptr<texture> a_texture, int32_t a_top, int32_t a_left, float a_scale)
	{
		if (a_texture)
			a_texture->render(m_renderer, a_top, a_left, a_scale);
	}


	std::shared_ptr<texture> renderer::render_text(std::shared_ptr<font> a_font, const std::string& a_text, float a_red, float a_green, float a_blue, float a_alpha)
	{
		if (a_font)
			return a_font->render(m_renderer, a_text, a_red, a_green, a_blue, a_alpha);

		return std::shared_ptr<texture>();
	}


	void renderer::set_clip_rect(int32_t a_top, int32_t a_left, int32_t a_width, int32_t a_height)
	{
		SDL_Rect clip_rect;

		clip_rect.h = a_height;
		clip_rect.w = a_width;
		clip_rect.x = a_left;
		clip_rect.y = a_top;

		if (SDL_RenderSetClipRect(m_renderer, &clip_rect) != 0)
			throw sdl_error(std::string("failed to set clip rectangle"));
	}


	void renderer::clear_clip_rect(void)
	{
		if (SDL_RenderSetClipRect(m_renderer, nullptr) != 0)
			throw sdl_error(std::string("failed to clear clip rectangle"));
	}

}