#pragma once

#include "point.hpp"

#include <memory>

namespace game
{

	class board;
	class board_view;

	class board_controller
	{
	public:
		board_controller(int a_top, int a_left, int a_slot_size, float a_mouse_click_threshold);

		void update(board& a_board);

		void handle_mouse_drag(board& a_board, const point<int>& a_start, const point<int>& a_last);

		void handle_mouse_up(board& a_board, const point<int>& a_last, float a_time_pressed);

		bool has_selected_gem(void) const { return m_selected_gem.m_x >= 0; }

		const point<int>& get_last_selected_gem(void) const { return m_selected_gem; }

	protected:
		void try_swap(board& a_board, const point<int>& a_from, const point<int>& a_to);

	private:
		point<int> m_selected_gem;

		bool m_can_drag_swap;			// true if drag swapping gems is possible
		bool m_is_swapping;
		point<int> m_swapping_slots[2];

		int m_top;
		int m_left;
		int m_slot_size;

		float m_mouse_click_threshold;
	};
}