
#include "game_driver.hpp"
#include "gem_generator.hpp"
#include "gem_matcher.hpp"

#include <algorithm>

namespace game
{

	game_driver::game_driver(std::shared_ptr<gem_generator> a_gem_generator)
		: m_gem_generator(a_gem_generator)
		, m_drop_acceleration(10.0f)
		, m_gem_drop_speed(1.0f)
		, m_gem_vanishing_speed(1.0f)
		, m_score(0)
		, m_num_landed(0)
	{}

	game_driver::game_driver(std::shared_ptr<gem_generator> a_gem_generator, float a_drop_speed, float a_vanish_speed)
		: m_gem_generator(a_gem_generator)
		, m_drop_acceleration(10.0f)
		, m_gem_drop_speed(a_drop_speed)
		, m_gem_vanishing_speed(a_vanish_speed)
		, m_score(0)
	{}

	void game_driver::step_time(board& a_board, float a_dt)
	{
		m_num_landed = 0;

		for (size_t col = 0; col < a_board.get_width(); col++)
			update_reverse(a_board.begin_column(col), a_board.last_column(col), a_board.get_height(), a_dt);

		// look for gems to vanish
		gem_matcher matcher;
		uint32_t score;
		
		if (m_gem_generator)
			m_gems_matched.assign(m_gem_generator->get_num_gems(), 0);

		matcher.match(a_board);

		// remove gems and count score
		for (auto s = a_board.begin(); s != a_board.end(); s++)
		{
			score = s->get_score(3);
			if (score >= 3)
			{
				// 3 gems give 30 point each, 4 gems give 60 points each, 5 gems give 120 points each and so on
				m_score += std::max(1U, 2 * (score - 3)) * 30;
				s->set_vanishing(s->get_gem(), 1.0f);

				if (m_gem_generator)
				{
					m_gems_matched[s->get_gem()] = std::max(m_gems_matched[s->get_gem()], score);
				}
			}
		}
	}


	void game_driver::update_reverse(board::slot_iterator a_from, board::slot_iterator a_to, uint32_t a_columnstride, float a_dt)
	{
		const float vanishing_distance = m_gem_vanishing_speed * a_dt;
		const float drop_distance = m_gem_drop_speed * a_dt;
		board::slot_iterator current, next(a_to);
		float distance;

		do
		{
			current = next;

			if (next != a_from)
				next--;

			switch (current->get_state())
			{
			case slot::state::empty:
				break;

			case slot::state::resting:
				if (current != a_to)
				{
					board::slot_iterator prev(current + 1);
					if (prev->is_moving() || prev->is_empty())
						move_gem(current, a_to, a_dt);
				}
				break;

			case slot::state::vanishing:
				distance = current->get_distance() - vanishing_distance;
				if (distance > 0.0f)
				{
					current->set_distance(distance);
				}
				else
				{
					current->set_empty();
					if (next->has_gem())
					{
						// set the resting gem in the slot above as moving into it with a distance
						// such that it in the next iteration will be moved into the current slot
						// while only using the fraction of the time step it has actually been moving
						const float delta = -m_drop_acceleration * (1.0f + distance / vanishing_distance) * a_dt;
						next->set_moving(next->get_gem(), 0.0f);
						next->set_velocity(delta);
					}
				}
				break;

			case slot::state::moving:
				move_gem(current, a_to, a_dt);
				break;
			}

		} while (current != a_from);

		if (current->is_empty() && m_gem_generator)
		{
			// generate a gem and add it as moving
			current->set_moving(m_gem_generator->next_gem(), 1.0f);
		}
		
	}


	void game_driver::move_gem(board::slot_iterator a_slot, board::slot_iterator a_last, float a_dt)
	{
		board::slot_iterator next = a_slot;
		board::slot_iterator destination = a_slot;		

		if (a_slot->has_gem())
			a_slot->set_moving(a_slot->get_gem(), 0.0f);

		float velocity;

		if (!a_slot->is_swapping())
		{
			velocity = a_slot->get_velocity() + m_drop_acceleration * a_dt;
			a_slot->set_velocity(velocity);
		}
		else
		{
			velocity = m_gem_drop_speed;
		}

		float distance = a_slot->get_distance() - velocity * a_dt;

		// if we are not swapping but falling we handle fall through to slots below
		if (!a_slot->is_swapping() && a_slot != a_last)
		{
			const uint32_t gem = a_slot->get_gem();

			do
			{
				next += 1;
				if (next->is_empty() && distance < 0.0f)
				{
					destination = next;
					distance += 1.0f;
				}
				else if (next->is_moving())
				{
					distance = std::max(distance, next->get_distance());
				}
			} while (distance < 0.0f && next != a_last);
		}

		if (distance <= 0.0f)
		{
			destination->set_gem(a_slot->get_gem());
			m_num_landed++;
		}
		else if (destination != a_slot)
		{
			destination->set_moving(a_slot->get_gem(), distance);
			destination->set_velocity(a_slot->get_velocity());
		}
		else
		{
			destination->set_distance(distance);
		}

		while (a_slot != destination)
		{
			a_slot->set_empty();
			a_slot += 1;
		}
	}


	uint32_t game_driver::get_num_gems_matched(size_t a_gem) const
	{
		if (a_gem >= 0 && a_gem < m_gems_matched.size())
			return m_gems_matched[a_gem];
		return 0;
	}
}