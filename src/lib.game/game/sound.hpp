#pragma once

struct Mix_Chunk;

namespace game
{

	class sound
	{
	public:
		sound(Mix_Chunk* a_chunk);

		~sound(void);

		void play(void);

	private:
		Mix_Chunk * m_chunk;
	};
}