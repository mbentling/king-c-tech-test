
#include "settings.hpp"

#include <fstream>
#include <stdexcept>

#include <stdio.h>


namespace game
{

	settings::settings(std::vector<std::string>&& a_cli_arguments)
		: m_cli_arguments(std::move(a_cli_arguments))
		, m_window_width(755)	// width of background texture
		, m_window_height(600)	// height of background texture
		, m_background_texture(std::string("BackGround.jpg"))
		, m_gem_textures({ std::string("Blue.png"), std::string("Green.png"), std::string("Purple.png"), std::string("Red.png"), std::string("Yellow.png") })
		, m_gem_match_sounds({ std::string("match_blue.wav"), std::string("match_green.wav"), std::string("match_purple.wav"), std::string("match_red.wav"), std::string("match_yellow.wav") })
		, m_gem_drop_sound("gem_drop.wav")
		, m_match_threshold(3)
		, m_mouse_click_threshold(0.2f)
		, m_board_top(100)
		, m_board_left(335)
		, m_slot_width(42)
		, m_top_margin(7)
		, m_font("BADABB__.ttf")
		, m_num_gems_horizontal(8)
		, m_num_gems_vertical(8)
		, m_game_time_seconds(60.0f)
		, m_title_music("Run_Amok.ogg")
		, m_game_music("Monkeys_Spinning_Monkeys.ogg")
		, m_game_end_music("Circus_Tent.ogg")
		, m_highscore(12060)	// my personal best so far :D
		, m_score_position(45, 45)
	{
		m_game_root = find_game_root_path();
		m_assets_path = m_game_root + "/assets";
	}


	uint32_t settings::get_window_width(void) const
	{
		return m_window_width;
	}


	uint32_t settings::get_window_height(void) const
	{
		return m_window_height;
	}


	std::string settings::get_background_texture(void) const
	{
		return get_images_path() + "/" + m_background_texture;
	}


	std::string settings::get_title_music(void) const 
	{ 
		return get_sounds_path() + "/" + m_title_music;
	}


	std::string settings::get_game_music(void) const 
	{ 
		return get_sounds_path() + "/" + m_game_music;
	}


	std::string settings::get_game_end_music(void) const 
	{ 
		return get_sounds_path() + "/" + m_game_end_music;
	}


	size_t settings::get_num_gems(void) const
	{
		return m_gem_textures.size();
	}


	uint32_t settings::get_match_threshold(void) const
	{
		return m_match_threshold;
	}


	float settings::get_mouse_click_threshold(void) const
	{
		return m_mouse_click_threshold;
	}


	std::string settings::get_gem_texture(size_t a_gem_no) const
	{
		return get_images_path() + "/" + m_gem_textures.at(a_gem_no);
	}


	std::string settings::get_gem_match_sound(size_t a_gem_no) const
	{
		return get_sounds_path() + "/" + m_gem_match_sounds.at(a_gem_no);
	}


	std::string settings::get_gem_drop_sound(void) const
	{
		return get_sounds_path() + "/" + m_gem_drop_sound;
	}


	std::string settings::get_font(void) const 
	{
		return get_fonts_path() + "/" + m_font; 
	}


	const std::string& settings::get_assets_path(void) const
	{
		return m_assets_path;
	}


	std::string settings::get_images_path(void) const
	{
		return m_assets_path + "/images";
	}


	std::string settings::get_sounds_path(void) const
	{
		return m_assets_path + "/sounds";
	}


	std::string settings::get_fonts_path(void) const
	{
		return m_assets_path + "/fonts";
	}

	/**
	 * Searches for the game root folder
	 * Would usually be configured but this is independent of build, unit tests etc due to project layout
	 *
	 * @param a_folder A folder the game root folder should contain
	 */
	std::string settings::find_game_root_path(void)
	{
		std::string::size_type end = std::string::npos;
		std::string path(get_binary_directory());
		std::string filename;

		filename = path + "/assets/images/" + m_background_texture;

		while (!std::ifstream(filename.c_str()))
		{
			end = path.rfind('/', end - 1);
			if (end == std::string::npos)
				throw std::runtime_error("expected a 'bin' parent folder in working directory path %s");

			path.resize(end);
			filename = path + "/assets/images/" + m_background_texture;
		}

		path.resize(end);
		return path;
	}


	std::string settings::get_binary_directory(void)
	{
		if (!m_cli_arguments.empty())
		{
			std::string path = m_cli_arguments[0];
			std::string::size_type n = path.find_last_of('/');

			if (n != std::string::npos)
				return path.substr(0, n);

			n = path.find_last_of('\\');
			if (n != std::string::npos)
			{
				path = path.substr(0, n);
				for (auto ch = path.begin(); ch != path.end(); ch++)
				{
					if (*ch == '\\')
						*ch = '/';
				}
				return path;
			}
		}

		throw std::runtime_error("cannot determine binary directory");
	}

}