#pragma once

#include "board.hpp"
#include "board_controller.hpp"
#include "game_driver.hpp"
#include "gem_generator.hpp"
#include "settings.hpp"
#include "window.hpp"

#include <vector>

namespace game
{

	class board_view;
	class texture;

	class game
	{
	public:
		game(const settings& a_settings);

		~game(void);

		void run(void);

	private:
		window m_window;
		settings m_settings;
	};

}