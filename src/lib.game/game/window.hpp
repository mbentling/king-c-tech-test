#pragma once

#include "point.hpp"

#include <memory>
#include <string>

struct SDL_Window;
struct SDL_Renderer;
struct SDL_MouseButtonEvent;

namespace game
{

	class renderer;

	class mouse_state
	{
	public:
		mouse_state(void)
			: m_is_down(false)
			, m_time_pressed(0.0f)
		{}

		bool is_down(void) const { return m_is_down; }

		void button_down(const point<int>& a_pos)
		{
			if (m_is_down)
			{
				m_last = a_pos;
			}
			else
			{
				m_is_down = true;
				m_start = a_pos;
				m_last.set(-1, -1);
			}
			m_time_pressed = 0.0f;
		}

		void button_released(const point<int>& a_last, float a_time_pressed)
		{
			m_is_down = false;
			m_last = a_last;
			m_time_pressed = a_time_pressed;
		}

		void move(const point<int>& a_last, float a_time_pressed)
		{
			if (m_is_down)
			{
				m_last = a_last;
				m_time_pressed = a_time_pressed;
			}
		}

		inline const point<int>& get_start(void) const { return m_start; }

		inline const point<int>& get_last(void) const { return m_last; }

		float get_time_pressed_seconds(void) const { return m_time_pressed; }

	private:
		point<int> m_start;
		point<int> m_last;
		bool m_is_down;
		float m_time_pressed;
	};

	class window
	{
	public:
	public:
		window(uint32_t a_width, uint32_t a_height, bool a_show);
		
		~window(void);

		void set_title(const std::string& a_title);

		bool exhaust_events(void);

		std::weak_ptr<renderer> get_renderer(void);

		const mouse_state& get_mouse_state(void) const { return m_mouse_state; }

	private:
		SDL_Window* m_window;
		std::shared_ptr<renderer> m_renderer;
		mouse_state m_mouse_state;
		uint64_t m_mouse_button_start;
	};

}