#pragma once

#include "board.hpp"
#include "gem_generator.hpp"

#include <memory>

namespace game
{

	class game_driver
	{
	public:
		game_driver(std::shared_ptr<gem_generator> a_gem_generator = std::shared_ptr<gem_generator>());
		
		game_driver(std::shared_ptr<gem_generator> a_gem_generator, float a_drop_speed, float a_vanish_speed);

		void step_time(board& a_board, float a_dt);

		void update_reverse(board::slot_iterator a_from, board::slot_iterator a_to, uint32_t a_columnstride, float a_distance);

		float get_drop_acceleration(void) const { return m_drop_acceleration; }

		uint32_t get_score(void) const { return m_score; }

		uint32_t get_num_landed(void) const { return m_num_landed; }

		uint32_t get_num_gems_matched(size_t a_gem) const;

	protected:
		void move_gem(board::slot_iterator a_slot, board::slot_iterator a_last, float a_dt);

	private:
		std::shared_ptr<gem_generator> m_gem_generator;
		float m_drop_acceleration;
		float m_gem_drop_speed;
		float m_gem_vanishing_speed;
		uint32_t m_score;

		uint32_t m_num_landed;

		std::vector<uint32_t> m_gems_matched;
	};
}