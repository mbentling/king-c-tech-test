
#include "board.hpp"

#include "point.hpp"

#include <algorithm>
#include <random>

namespace game
{

	board::board(void)
		: m_width(0)
		, m_height(0)
	{
	}


	board::board(size_t a_width, size_t a_height)
	{
		reset(a_width, a_height);
	}


	void board::reset(size_t a_width, size_t a_height)
	{
		m_slots.resize(a_width * a_height, slot());
		m_width = a_width;
		m_height = a_height;
	}


	bool board::is_at_rest(void) const
	{
		for (auto s = m_slots.begin(); s != m_slots.end(); s++)
		{
			if (s->get_state() != slot::state::resting)
				return false;
		}

		return true;
	}
}