#pragma once

#include "point.hpp"

#include <cstdint>
#include <cassert>
#include <iostream>

namespace game
{

	class slot
	{
	public:
		enum class state
		{
			empty,		// the slot is empty and a gem can begin to move into it
			resting,	// a gem is resting in the slot and is available for matching
			moving,		// a gem is moving into the slot
			vanishing	// a gem has been matched in the slot and is now vanishing
		};

		enum axis 
		{ 
			axis_x = 0, // slot score for gems matched in the same row
			axis_y = 1	// slot score for gems matched in the same column
		};

		enum swap_direction
		{
			none = 0,
			north,
			east,
			south,
			west
		};

		typedef uint8_t gem_t;

	public:
		slot(void)
			: m_state(state::empty)
			, m_swap_from(none)
		{}

		slot(gem_t a_gem, state a_state, float a_distance)
			: m_state(a_state)
			, m_gem(a_gem)
			, m_swap_from(none)
			, m_distance(a_distance)
			, m_velocity(0.0f)
		{
			assert(m_state == state::moving || m_state == state::vanishing);
		}

		slot(gem_t a_gem, uint8_t a_score_x, uint8_t a_score_y)
			: m_state(state::resting)
			, m_gem(a_gem)
			, m_swap_from(none)
			, m_score_x(a_score_x)
			, m_score_y(a_score_y)
			, m_velocity(0.0f)
		{}

		inline void set_gem(gem_t a_gem)
		{
			m_gem = a_gem;
			m_score_x = 0;
			m_score_y = 0;
			m_state = state::resting;
			m_velocity = 0.0f;
		}

		inline void set_score(uint8_t a_score, axis a_axis)
		{
			if (a_axis == axis_x)
				m_score_x = a_score;
			else
				m_score_y = a_score;
		}

		inline bool is_empty(void) const { return m_state == state::empty; }

		inline bool is_moving(void) const { return m_state == state::moving && m_swap_from == none; }

		inline bool is_swapping(void) const { return m_state == state::moving && m_swap_from != none; }

		inline bool has_gem(void) const { return m_state == state::resting; }

		inline bool has_gem(gem_t a_gem) const { return has_gem() && m_gem == a_gem; }

		inline gem_t get_gem(void) const { return m_gem; }

		inline state get_state(void) const { return m_state; }

		inline float get_distance(void) const { return m_distance; }

		inline float get_velocity(void) const { return m_velocity; }

		inline swap_direction get_swap_from(void) const { return static_cast<swap_direction>(m_swap_from); }

		inline uint32_t get_score(uint32_t a_threshold) const 
		{ 
			uint32_t score = 0;

			if (m_score_x >= a_threshold)
				score += m_score_x;

			if (m_score_y >= a_threshold)
				score += m_score_y;

			return score;
		}

		inline void set_distance(float a_distance)
		{
			m_distance = a_distance;
		}

		inline void set_moving(gem_t a_gem, float a_distance)
		{
			m_gem = a_gem;
			m_state = state::moving;
			m_swap_from = none;
			m_distance = a_distance;
		}

		inline void set_vanishing(gem_t a_gem, float a_distance)
		{
			m_gem = a_gem;
			m_state = state::vanishing;
			m_distance = a_distance;
		}

		inline void set_swapping(gem_t a_gem, swap_direction a_swap_from, float a_distance)
		{
			m_gem = a_gem;
			m_state = state::moving;
			m_swap_from = a_swap_from;
			m_distance = a_distance;
		}

		inline void set_empty(void) { m_state = state::empty; }

		inline void set_velocity(float a_velocity) { m_velocity = a_velocity; }

		bool operator==(const slot& a_slot) const;

		friend std::ostream& operator<<(std::ostream& a_os, const slot& a_slot);
		
		template <class T>
		static swap_direction get_swap_direction(const point<T>& a_from, const point<T>& a_to)
		{
			if (a_from.m_x < a_to.m_x && a_from.m_y == a_to.m_y)
				return west;
			else if (a_from.m_x > a_to.m_x && a_from.m_y == a_to.m_y)
				return east;
			else if (a_from.m_x == a_to.m_x && a_from.m_y < a_to.m_y)
				return north;
			else if (a_from.m_x == a_to.m_x && a_from.m_y > a_to.m_y)
				return south;
			return none;
		}

	private:
		state m_state;			// slot state
		float m_distance;		// how far a moving or vanishing state is from completing (when it reaches zero)
		float m_velocity;		// velocity of a moving gem
		gem_t m_gem;			// type of gem moving to or stored in slot
		uint8_t m_swap_from;	// the direction we are swapping from
		uint8_t m_score_x;		// gem sequence score (i.e. how many of the same gem in a row sequence this slot is a part of)
		uint8_t m_score_y;		// gem sequence score (i.e. how many of the same gem in a column sequence this slot is a part of)
	};


	std::ostream& operator<<(std::ostream& a_os, const slot::state& a_state);

}