#pragma once

#include <memory>

namespace game
{

	class settings;
	class window;

	class game_state
	{
	public:
		game_state(void) {}

		virtual ~game_state(void) {}

		virtual std::shared_ptr<game_state> update(window& a_window, const settings& a_settings, float a_dt) = 0;
	};

}