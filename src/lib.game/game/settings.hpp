#pragma once

#include "point.hpp"

#include <cstdint>
#include <string>
#include <vector>

namespace game
{

	class settings
	{
	public:
		settings(std::vector<std::string>&& cli_arguments = {});

		uint32_t get_window_width(void) const;

		uint32_t get_window_height(void) const;

		std::string get_background_texture(void) const;

		size_t get_num_gems(void) const;

		uint32_t get_match_threshold(void) const;

		float get_mouse_click_threshold(void) const;

		std::string get_gem_texture(size_t a_gem_no) const;

		std::string get_gem_match_sound(size_t a_gem_no) const;

		std::string get_gem_drop_sound(void) const;

		const std::string& get_assets_path(void) const;

		std::string get_images_path(void) const;

		std::string get_sounds_path(void) const;

		std::string get_fonts_path(void) const;

		std::string find_game_root_path(void);

		std::string get_binary_directory(void);

		uint32_t get_board_top(void) const { return m_board_top; }

		uint32_t get_board_left(void) const { return m_board_left; }

		uint32_t get_slot_width(void) const { return m_slot_width; }

		uint32_t get_top_margin(void) const { return m_top_margin; }

		std::string get_font(void) const;

		uint32_t get_num_gems_horizontal(void) const { return m_num_gems_horizontal; }

		uint32_t get_num_gems_vertical(void) const { return m_num_gems_vertical; }

		float get_game_time(void) const { return m_game_time_seconds; }

		std::string get_title_music(void) const;

		std::string get_game_music(void) const;

		std::string get_game_end_music(void) const;

		uint32_t get_highscore(void) const { return m_highscore; }

		point<uint32_t> get_score_position(void) const { return m_score_position; }

	private:
		std::vector<std::string> m_cli_arguments;

		std::string m_background_texture;
		std::vector<std::string> m_gem_textures;
		std::vector<std::string> m_gem_match_sounds;
		std::string m_gem_drop_sound;

		std::string m_game_root;
		std::string m_assets_path;
		uint32_t m_window_width;
		uint32_t m_window_height;
		uint32_t m_match_threshold;
		float m_mouse_click_threshold;
		
		uint32_t m_board_top;	// top coordinate of where the board will be rendered
		uint32_t m_board_left;	// left coordinate of where the board will be rendered
		uint32_t m_slot_width;	// the width of a rendered board slot
		uint32_t m_top_margin;	// top margin used when rendering new gems falling into the board

		uint32_t m_num_gems_horizontal;
		uint32_t m_num_gems_vertical;

		float m_game_time_seconds;

		std::string m_font;

		std::string m_title_music;
		std::string m_game_music;
		std::string m_game_end_music;

		uint32_t m_highscore;
		point<uint32_t> m_score_position;
	};

}