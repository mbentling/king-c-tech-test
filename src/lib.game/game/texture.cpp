
#include "texture.hpp"

#include "exception.hpp"

#include <SDL.h>

namespace game
{

	texture::texture(SDL_Texture* a_texture)
		: m_texture(a_texture)
	{
		Uint32 format;
		int  access;

		SDL_assert(m_texture != nullptr);
		if (SDL_QueryTexture(m_texture, &format, &access, &m_width, &m_height) != 0)
			throw sdl_error(std::string("failed to query texture dimensions"));
	}


	texture::~texture(void)
	{
		SDL_DestroyTexture(m_texture);
	}

	void texture::render(SDL_Renderer* a_renderer, int32_t a_top, int32_t a_left, float a_scale)
	{
		if (a_renderer == nullptr)
			throw std::invalid_argument("nullpointer");

		SDL_Rect dst;

		dst.h = m_height;
		dst.w = m_width;
		dst.x = a_left;
		dst.y = a_top;

		if (a_scale != 1.0f)
		{
			dst.h = static_cast<int>(dst.h * a_scale + 0.5f);
			dst.w = static_cast<int>(dst.w * a_scale + 0.5f);
		}

		SDL_RenderCopy(a_renderer, m_texture, NULL, &dst);
	}

}