
#include "game_play.hpp"

#include "board_view.hpp"
#include "game_title.hpp"
#include "gem_generator.hpp"
#include "music.hpp"
#include "renderer.hpp"
#include "settings.hpp"
#include "sound.hpp"
#include "texture.hpp"

#include <algorithm>
#include <sstream>

namespace game
{

	game_play::game_play(window& a_window, const settings& a_settings, uint32_t a_highscore)
		: m_gem_generator(std::make_shared<gem_generator>(a_settings.get_num_gems()))
		, m_board_controller(a_settings.get_board_top(), a_settings.get_board_left(), a_settings.get_slot_width(), a_settings.get_mouse_click_threshold())
		, m_game_driver(m_gem_generator, 4.0f, 4.0f)
		, m_elapsed_time(0.0f)
		, m_title_delay(3.0f)
		, m_highscore(a_highscore)
	{
		std::shared_ptr<renderer> renderer(a_window.get_renderer());

		// load font
		m_font = renderer->load_font(a_settings.get_font(), 24);

		// load background
		m_background = renderer->load_texture(a_settings.get_background_texture());

		// load gem textures and sound effects
		std::vector<std::weak_ptr<texture>> gem_textures;

		gem_textures.reserve(a_settings.get_num_gems());
		m_match_sounds.reserve(a_settings.get_num_gems());

		for (size_t gem = 0; gem < a_settings.get_num_gems(); gem++)
		{
			std::weak_ptr<texture> gem_texture = renderer->load_texture(a_settings.get_gem_texture(gem));
			gem_textures.push_back(gem_texture);

			std::weak_ptr<sound> sfx = m_sound_mixer.get_sound(a_settings.get_gem_match_sound(gem));
			m_match_sounds.push_back(sfx);
		}

		m_drop_sound = m_sound_mixer.get_sound(a_settings.get_gem_drop_sound());

		// create board view
		m_board_view = std::make_unique<board_view>(std::move(gem_textures), a_settings.get_board_top(), a_settings.get_board_left(), a_settings.get_slot_width(), a_settings.get_top_margin());

		// generate board
		m_board.reset(8, 8);
		m_gem_generator->fill_board(m_board, a_settings.get_match_threshold());

		// load music
		m_music_start = m_sound_mixer.get_music(a_settings.get_game_music());
		m_music_end = m_sound_mixer.get_music(a_settings.get_game_end_music());

		// start music
		std::vector<std::weak_ptr<sound>> m_match_sounds;
		std::shared_ptr<music> to_play(m_music_start);
		to_play->play();
	}

	game_play::~game_play(void)
	{}


	std::shared_ptr<game_state> game_play::update(window& a_window, const settings& a_settings, float a_dt)
	{
		// end game criteria
		if (is_time_up(a_settings) && m_board.is_at_rest())
		{
			m_title_delay -= a_dt;
			if (m_title_delay <= 0.0f)
			{
				uint32_t score = m_game_driver.get_score();
				return std::make_shared<game_title>(a_window, a_settings, std::max(score, m_highscore), score);
			}
		}

		// change music if we are halfway
		if (should_change_music(a_settings, a_dt, 0.5f))
		{
			std::shared_ptr<music> to_play(m_music_end);
			m_sound_mixer.stop_music();
			to_play->play(1);
		}

		handle_input(a_window, a_settings);

		step_time(a_dt);

		play_sounds(a_settings);

		render_state(a_window, a_settings, a_dt);

		return std::shared_ptr<game_state>();
	}


	void game_play::step_time(float a_dt)
	{
		m_game_driver.step_time(m_board, a_dt);
		m_elapsed_time += a_dt;
	}


	void game_play::render_state(window& a_window, const settings& a_settings, float a_dt)
	{
		// we also always render the game state
		std::shared_ptr<renderer> renderer(a_window.get_renderer());

		renderer->clear_clip_rect();
		renderer->render(std::shared_ptr<texture>(m_background), 0, 0);

		m_board_view->render(renderer, m_board, m_board_controller, a_dt);

		std::shared_ptr<font> fnt(m_font);

		render_text(*renderer, a_settings);

		renderer->flush();
	}


	void game_play::render_text(renderer& a_renderer, const settings& a_settings)
	{
		std::shared_ptr<font> fnt(m_font);
		const point<uint32_t> score_pos(a_settings.get_score_position());
		std::shared_ptr<texture> label;
		std::ostringstream os;

		// render score
		os << "Score: " << m_game_driver.get_score();
		label = a_renderer.render_text(fnt, os.str(), 1.0f, 1.0f, 1.0f, 1.0f);
		a_renderer.render(label, score_pos.m_x, score_pos.m_y);

		if (is_time_up(a_settings))
		{
			label = a_renderer.render_text(fnt, "Time up!", 1.0f, 1.0f, 1.0f, 1.0f);

			const int top = a_settings.get_board_top() + (a_settings.get_num_gems_vertical() * a_settings.get_slot_width() - label->get_height()) / 2;
			const int left = a_settings.get_board_left() + (a_settings.get_num_gems_horizontal() * a_settings.get_slot_width() - label->get_width()) / 2;

			a_renderer.render(label, top, left);
		}
	}


	bool game_play::should_change_music(const settings& a_settings, float a_dt, float a_change_at)
	{
		return m_elapsed_time < a_settings.get_game_time() * a_change_at && m_elapsed_time + a_dt > a_settings.get_game_time() * a_change_at;
	}


	void game_play::play_sounds(const settings& a_settings)
	{
		for (size_t gem = 0; gem < a_settings.get_num_gems(); gem++)
		{
			if (m_game_driver.get_num_gems_matched(gem) > 0 && gem < m_match_sounds.size())
			{
				std::shared_ptr<sound> sfx(m_match_sounds[gem]);
				sfx->play();
			}
		}

		if (m_game_driver.get_num_landed())
		{
			std::shared_ptr<sound> sfx(m_drop_sound);
			sfx->play();
		}
	}


	bool game_play::mouse_released(const window& a_window) const
	{
		return m_last_mouse_state.is_down() && !a_window.get_mouse_state().is_down();
	}


	bool game_play::is_time_up(const settings& a_settings) const
	{
		return m_elapsed_time >= a_settings.get_game_time();
	}

	void game_play::handle_input(window& a_window, const settings& a_settings)
	{
		const mouse_state& mouse = a_window.get_mouse_state();

		if (!is_time_up(a_settings))
		{
			m_board_controller.update(m_board);

			if (m_last_mouse_state.is_down())
			{
				if (mouse.is_down())
					m_board_controller.handle_mouse_drag(m_board, mouse.get_start(), mouse.get_last());
				else
					m_board_controller.handle_mouse_up(m_board, mouse.get_last(), mouse.get_time_pressed_seconds());
			}
		}

		m_last_mouse_state = mouse;
	}
}