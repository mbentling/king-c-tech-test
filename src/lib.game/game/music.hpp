#pragma once

struct _Mix_Music;
typedef _Mix_Music Mix_Music;

namespace game
{

	class music
	{
	public:
		music(Mix_Music* a_music);
		
		~music(void);

		void play(int a_num_loops = -1);

	private:
		Mix_Music* m_music;
	};
}