
#include "gem_generator.hpp"

#include "board.hpp"

#include <SDL_timer.h>

namespace game
{

	gem_generator::gem_generator(uint32_t a_num_gems)
		: m_gem_distribution(0, a_num_gems - 1)
		, m_generator(static_cast<uint32_t>(SDL_GetPerformanceCounter()))
		, m_num_gems(a_num_gems)
	{}


	/**
	 * Fill the board without generating a_sequence_limit or more of the same type in a row
	 * We fill each column from the top to the bottom starting with the left most column
	 * The count methods only consider gems to the left and above the gem it is counting score for
	 */
	void gem_generator::fill_board(board& a_board, uint32_t a_sequence_limit)
	{
		const size_t height = a_board.get_height();
		const size_t width = a_board.get_width();

		for (size_t col = 0; col < width; col++)
		{
			for (size_t row = 0; row < height; row++)
			{
				do
				{
					a_board.set_slot(col, row, slot(next_gem(), 0, 0));
				} while (count_row_sequence(a_board, col, row) >= a_sequence_limit || count_column_sequence(a_board, col, row) >= a_sequence_limit);
			}
		}
	}


	/**
	 * This only counts gems to the left of [col,row]
	 */
	uint32_t gem_generator::count_row_sequence(board& a_board, uint32_t a_col, uint32_t a_row)
	{
		board::slot_iterator current, end;
		uint32_t gem, num = 1;

		current = a_board.at(a_col, a_row);
		end = a_board.at(0, a_row);
		gem = current->get_gem();

		while (current != end)
		{
			current = a_board.left(current);
			if (current->get_gem() != gem)
				return num;
			else
				num++;
		}

		return num;
	}

	/**
	* This only counts gems above [col,row]
	*/
	uint32_t gem_generator::count_column_sequence(board& a_board, uint32_t a_col, uint32_t a_row)
	{
		board::slot_iterator current, end, next;
		uint32_t gem, num = 1;

		current = a_board.at(a_col, a_row);
		end = a_board.at(a_col, 0);
		gem = current->get_gem();

		while (current != end)
		{
			current = a_board.up(current);
			if (current->get_gem() != gem)
				return num;
			else
				num++;
		}

		return num;
	}


}