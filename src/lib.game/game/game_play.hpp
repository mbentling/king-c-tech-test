#pragma once

#include "game_state.hpp"

#include "board.hpp"
#include "board_controller.hpp"
#include "game_driver.hpp"
#include "sound_mixer.hpp"
#include "window.hpp"

namespace game
{

	class board_view;
	class font;
	class gem_generator;
	class settings;
	class texture;

	class game_play : public game_state
	{
	public:
		game_play(window& a_window, const settings& a_settings, uint32_t a_highscore);

		virtual ~game_play(void);

		virtual std::shared_ptr<game_state> update(window& a_window, const settings& a_settings, float a_dt);

		bool is_time_up(const settings& a_settings) const;

		bool mouse_released(const window& a_window) const;

	protected:
		void handle_input(window& a_window, const settings& a_settings);

		void step_time(float a_dt);

		void render_state(window& a_window, const settings& a_settings, float a_dt);

		void render_text(renderer& a_renderer, const settings&_a_settings);

		bool should_change_music(const settings& a_settings, float a_dt, float a_change_at);

		void play_sounds(const settings& a_settings);

	private:
		std::shared_ptr<gem_generator> m_gem_generator;
		std::shared_ptr<board_view> m_board_view;
		std::shared_ptr<board_view> m_text_font;

		std::weak_ptr<music> m_music_start;	// music to play in the beginning
		std::weak_ptr<music> m_music_end;		// music to play when the end is near

		std::weak_ptr<texture> m_background;

		std::weak_ptr<font> m_font;

		std::vector<std::weak_ptr<sound>> m_match_sounds; 
		std::weak_ptr<sound> m_drop_sound;

		sound_mixer m_sound_mixer;
		board m_board;
		board_controller m_board_controller;
		game_driver m_game_driver;
		mouse_state m_last_mouse_state;

		float m_elapsed_time;
		float m_title_delay;

		uint32_t m_highscore;
	};

}