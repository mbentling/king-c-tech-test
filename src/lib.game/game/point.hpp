#pragma once

namespace game
{

	template <class T>
	struct point
	{
		T m_x;
		T m_y;

		point(void)
		{}

		point(T a_x, T a_y)
			: m_x(a_x)
			, m_y(a_y)
		{}

		void set(T a_x, T a_y)
		{
			m_x = a_x;
			m_y = a_y;
		}

		bool operator==(const point& a_point) const
		{
			return m_x == a_point.m_x && m_y == a_point.m_y;
		}
	};

}