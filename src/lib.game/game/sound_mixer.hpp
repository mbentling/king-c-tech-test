#pragma once

#include <map>
#include <memory>
#include <string>

namespace game
{

	class music;
	class sound;

	class sound_mixer
	{
	public:
		sound_mixer(void);

		~sound_mixer(void);

		std::weak_ptr<music> get_music(const std::string& a_file);

		std::weak_ptr<sound> get_sound(const std::string& a_file);

		bool is_playing_music(void) const;

		void fade_out_music(float a_seconds);

		void stop_music(void);

	private:
		std::map<std::string, std::shared_ptr<music>> m_music;
		std::map<std::string, std::shared_ptr<sound>> m_sound;
	};
}