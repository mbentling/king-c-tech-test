
#include "font.hpp"

#include "exception.hpp"
#include "texture.hpp"

#include <SDL.h>
#include <SDL_ttf.h>

namespace game
{

	font::font(TTF_Font* a_font)
		: m_font(a_font)
	{}


	font::~font(void)
	{
		if (m_font != nullptr)
			TTF_CloseFont(m_font);
	}

	std::shared_ptr<texture> font::render(SDL_Renderer* a_renderer, const std::string& a_text, float a_red, float a_green, float a_blue, float a_alpha)
	{
		SDL_Color color;

		color.a = static_cast<uint8_t>(255 * a_alpha);
		color.r = static_cast<uint8_t>(255 * a_red);
		color.g = static_cast<uint8_t>(255 * a_green);
		color.b = static_cast<uint8_t>(255 * a_blue);

		TTF_SetFontOutline(m_font, 0);

		SDL_Surface *surf = TTF_RenderText_Blended(m_font, a_text.c_str(), color);
		if (surf == nullptr)
			throw sdl_error("failed to render text to surface");

		SDL_Texture *sdl_texture = SDL_CreateTextureFromSurface(a_renderer, surf);
		if (sdl_texture == nullptr)
			throw sdl_error("failed to create texture from surface");

		//Clean up the surface and font
		SDL_FreeSurface(surf);

		return std::make_shared<texture>(sdl_texture);
	}

}