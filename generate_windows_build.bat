echo off
if not exist build mkdir build
cd build
if not exist windows mkdir windows
cd windows
cmake -G "Visual Studio 12" ../../
cd ../..
