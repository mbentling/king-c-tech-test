
                  King C++ Tech Test

                    Game Developer

                   Magnus  Bentling


This is the readme.txt for King C++ Tech Test implemented
by Magnus Bentling. The specification for the assignment
is available in the repository at doc/GameDeveloperTest-C.docx

All source, assets, dependencies etc is available as a git
repository at the following url:

https://bitbucket.org/mbentling/king-c-tech-test.git

The high score when starting the game is my best score while testing
the game. Can you beat it?


Credits

All music has been downloaded from http://incompetech.com/music/royalty-free/

"Circus Tent" Kevin MacLeod (incompetech.com)
"Monkeys Spinning Monkeys" Kevin MacLeod (incompetech.com)
"Run Amok" Kevin MacLeod (incompetech.com) 
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/


Folder structure

./assets
	Contains all assets needed by the game.
	
./bin
	Built binaries except static libraries are placed here
	
./build
	Generated directory holding an out of source build tree
	
./dep
	Contains source for all dependencies except Direct-X. It is
	possible to build the game without Direct-X but expect
	sound latencies.
	
./doc
	Contains the specification for the assignment.
	
./prebuilt
	Contains prebuilt binaries
	
./src
	Contains all source written by me separated into three projects.


Building

Build files are generated using CMake. There is a script for generating
build files for Visual Studio 12. If you desire to generate build files
for another IDE or platform create a directory in ./build/

This delivery has been built with MSVC 2013 linking with Direct-X and
tested on Windows 7. It does not build with older releases of MSVC due
to missing C++11 features. If you desire to build with Direct-X you must
add an environmental variable named DXSDK_DIR holding the path to the
root of your Direct-X SDK install directory. If you desire to build
for a different platform than Windows you will most likely have to edit
one or two CMakeLists.txt for dependencies as not all came with one.

Remember, building without Direct-X support forces SDL to use the Windows
Multimedia API. Expect sound delays!


Projects

./src/app.game
	The game as an executable
	
./src/lib.game
	The game itself

./src/test.lib.game
	Unit tests for core functionality. No tests for user
	interface or interaction has been implemented.


A brief top-down design description

The application toggles between a game_title state and a game_play
state. This is facilitated by the game object.

Gems are distributed in a 2D slot matrix owned by a board, manipulated
by a board_controller and rendered by a board_view. This is the classic
MVC pattern which happened to fit quite nicely.

A slot can have four different states.
	- Empty: An empty slot
	- Resting: A slot with a non-moving gem inside of it
	- Moving: A slot with a gem moving into it
	- Vanishing: A slot with a gem being removed
	
There is no gem state other than the state of a slot. For each frame
time is stepped forward, moving gems may fall through a slot to the
slot below, a slot with a vanishing game may become empty and trigger
a resting slot above to move its resting gem etc.

All slot state changes and game logic are handled by game_driver. It
updates the board column by column from the bottom towards the top. It
is also responsible for keep track of the score and if gems has become
resting (i.e. landed on another gem) etc.

The gem_matcher assigns a score tuple to each slot representing the
number of gems in the horizontal and vertical unbroken chain of the same
color. Each gem in a four gem vertical chain has a score of four in
its y axis.

Resources are cached by the resource managers which is renderer for
textures and fonts, sound_mixer for music and sound effects. Both
game_title and game_play assume the renderer it uses during a frame
update is the same as when it was created and that no resources are
flushed out during their lifetime.

Exception handling is rudimentary. No exceptions at all is expected but
if one is caught during a frame the game will try to iterate once more
and if another exception is caught it will exit.
